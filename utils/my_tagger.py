
# tags my music

import os
import time

from beets.mediafile import MediaFile

music_extensions = (".ogg", ".mp3", ".wav", ".m4a", ".flac")

folder_genres = {
    "/ボーカロイド/": "vocaloid",
    "/東方アレンジ/": "touhou",
    "/IOSYS 東方/": "iosys",
    "/アニメ/": "anime",
    "ソードアート": "sword art online sao",
    "ゼロから始める": "rezero re:zero",
    "ご注文はうさぎですか": "gochiusa usagi",
    "ときめきポポロン": "gochiusa usagi chimametai",
    "ノーポイッ！": "gochiusa usagi",
    "ぽっぴんジャンプ": "gochiusa usagi chimametai",
    "/Soundtrack/": "soundtrack",
    "/FFXIV Music": "ffxiv final fantasy",
    "アーシャのアトリエ": "atelier ayesha",
    "シャリーのアトリエ": "atelier shallie",
    "エスカ&ロジーのアトリエ": "atelier escha logy",
    "トトリのアトリエ": "atelier totori",
    "メルルのアトリエ": "atelier meruru",
    "ロロナのアトリエ": "atelier rorona",
    "Persona 4 Arena": "persona 4 arena",
    "ニコニコ": "niconico"
}

file_genres = {
    "初音ミク": "hatsune miku",
    "鏡音リン": "kagamine rin"
}

def get_genres(name, genres_dict):
    genres = []
    for key, value in genres_dict.items():
        if key in name:
            genres.append(value)
    return genres

for root, dirnames, filenames in os.walk("."):
    current_folder_genres = get_genres(root, folder_genres)
    for filename in filenames:
        if os.path.splitext(filename)[1].lower() in music_extensions:
            current_file_genres = get_genres(filename, file_genres)
            tags = {}
            try:
                media_file = MediaFile(os.path.join(root, filename))
                tags["genre"] = " ".join(current_folder_genres + current_file_genres)
                if media_file.genre is not None:
                    tags["genre"] += " {}".format(media_file.genre)
                if media_file.title is None:
                    tags["title"] = filename
                print(root, filename)
                print(tags)
                media_file.update(tags)
                media_file.save()
            except Exception as e:
                print("Failed to write tags")
                print(e)
                input()
            # time.sleep(0.5)

