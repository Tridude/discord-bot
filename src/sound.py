
# plays sounds

import os

import shlex
import argparse
import math
import random

import asyncio
import discord
from discord.ext import commands

from beets.mediafile import MediaFile
from beets.library import Library

class VoiceEntry:
    def __init__(self, message, player):
        self.requester = message.author
        self.channel = message.channel
        self.player = player

    def __str__(self):
        fmt = "|{0.title}| - requested by {1.display_name}"
        duration = self.player.duration
        if duration:
            fmt = fmt + " [length: {0[0]:.0f}m {0[1]:.0f}s]".format(divmod(duration, 60))
        return fmt.format(self.player, self.requester)

class VoiceState:
    def __init__(self, bot):
        self.current = None
        self.voice = None
        self.bot = bot
        self.play_next_song = asyncio.Event()
        self.songs = asyncio.Queue()
        self.skip_votes = set() # a set of user_ids that voted
        self.audio_player = self.bot.loop.create_task(self.audio_player_task())

    def is_playing(self):
        if self.voice is None or self.current is None:
            return False
        player = self.current.player
        return not player.is_done()

    @property
    def player(self):
        return self.current.player

    def skip(self):
        self.skip_votes.clear()
        if self.is_playing():
            self.player.stop()

    def toggle_next(self):
        self.bot.loop.call_soon_threadsafe(self.play_next_song.set)

    async def audio_player_task(self):
        while True:
            self.play_next_song.clear()
            self.current = await self.songs.get()
            await self.bot.send_message(self.current.channel, "Now playing {}".format(self.current))
            self.current.player.start()
            await self.play_next_song.wait()



class SoundCog():
    """
    foo
    """

    def __init__(self, bot, config={}):
        self.bot = bot
        self.voice_states = {}
        self.config = config
        self.volume = 0.2

        try:
            self.beets_library = Library(self.config["library"], self.config["music"])
        except Exception as e:
            print("Failed to load beets library")
            print(e)
            self.beets_library = None

        self.beets_list_parser = argparse.ArgumentParser()
        self.beets_list_parser.add_argument("-p", "--page", type=int, default=1)
        self.beets_list_parser.add_argument("-c", "--count", type=int, default=10)
        self.beets_list_parser.add_argument("query", nargs="+")

        self.beets_play_parser = argparse.ArgumentParser()
        self.beets_play_parser.add_argument("-i", "--index", type=int, default=0)
        self.beets_play_parser.add_argument("-c", "--count", type=int, default=1)
        self.beets_play_parser.add_argument("--random", action="store_true")
        self.beets_play_parser.add_argument("query", nargs="+")

        if not discord.opus.is_loaded():
            discord.opus.load_opus("opus")

    def get_voice_state(self, server):
        state = self.voice_states.get(server.id)
        if state is None:
            state = VoiceState(self.bot)
            self.voice_states[server.id] = state
        return state

    async def create_voice_client(self, channel):
        voice = await self.bot.join_voice_channel(channel)
        state = self.get_voice_state(channel.server)
        state.voice = voice

    def __unload(self):
        for state in self.voice_states.values():
            try:
                state.audio_player.cancel()
                if state.voice:
                    self.bot.loop.create_task(state.voice.disconnect())
            except:
                pass

    @commands.command(pass_context=True, no_pm=True)
    async def join(self, ctx, *, channel : discord.Channel):
        """Joins a voice channel"""
        try:
            await self.create_voice_client(channel)
        except discord.ClientException:
            await self.bot.say("I'm already in a voice channel")
        except discord.InvalidArgument:
            await self.bot.say("That's not a voice channel")
        else:
            await self.bot.say("I joined {}".format(channel.name))

    @commands.command(pass_context=True, no_pm=True)
    async def summon(self, ctx):
        """Summons the bot to join your voice channel."""
        summoned_channel = ctx.message.author.voice_channel
        if summoned_channel is None:
            await self.bot.say("You are not in a voice channel.")
            return False
        state = self.get_voice_state(ctx.message.server)
        if state.voice is None:
            state.voice = await self.bot.join_voice_channel(summoned_channel)
        else:
            await state.voice.move_to(summoned_channel)

        return True

    def get_ffmpeg_player(self, state, file_path, title, duration, pipe=False):
        player = state.voice.create_ffmpeg_player(file_path, after=state.toggle_next, pipe=pipe)
        player.title = title
        player.duration = duration
        player.volume = self.volume
        return player

    async def queue_player(self, state, message, player):
        entry = VoiceEntry(message, player)
        await state.songs.put(entry)
        await self.bot.say("Enqueued {}".format(entry))

    @commands.command(pass_context=True, no_pm=True)
    async def play(self, ctx, file_name : str):
        """Plays a song.
        If there is a song currently in the queue, then it is
        queued until the next song is done playing.
        """
        state = self.get_voice_state(ctx.message.server)
        if state.voice is None:
            success = await ctx.invoke(self.summon)
            if not success:
                return
        try:
            if not file_name.startswith("/"):
                file_path = os.path.join("/home/nas/sounds/", file_name)
            else:
                file_path = file_name
            file_info = MediaFile(file_path)
            player = self.get_ffmpeg_player(state, file_path, file_info.title, file_info.length)
            # player = await state.voice.create_ytdl_player(song, ytdl_options=opts, after=state.toggle_next)
        except Exception as e:
            fmt = 'An error occurred while processing this request: ```py\n{}: {}\n```'
            await self.bot.send_message(ctx.message.channel, fmt.format(type(e).__name__, e))
        else:
            await self.queue_player(state, ctx.message, player)

    @commands.command(pass_context=True, no_pm=True)
    async def beetlist(self, ctx, *, message: str):
        if self.beets_library is None:
            await self.bot.send_message(ctx.message.channel, "My music library is not setup. I can't search it.")
            return
        try:
            args = self.beets_list_parser.parse_args(shlex.split(message))
        except SystemExit as e:
            await self.bot.say("I don't understand that.")
            return
        results = self.beets_library.items(args.query)
        if len(results) == 0:
            await self.bot.send_message(ctx.message.channel, "I didn't find any music.")
        else:
            message = []
            message.append("Found {} music. Page {} of {}".format(len(results), args.page, math.ceil(len(results)/args.count)))
            index_begin = args.count*(args.page-1)
            index_end = min(index_begin+args.count, len(results))
            for index in range(index_begin, index_end):
                message.append("{}: {}".format(index, results[index].title))
            await self.bot.send_message(ctx.message.channel, "\n".join(message))

    @commands.command(pass_context=True, no_pm=True)
    async def beetplay(self, ctx, *, message: str):
        if self.beets_library is None:
            await self.bot.send_message(ctx.message.channel, "My music library is not setup. I can't search it.")
            return
        try:
            args = self.beets_play_parser.parse_args(shlex.split(message))
        except SystemExit as e:
            await self.bot.say("I don't understand that.")
            return
        #
        state = self.get_voice_state(ctx.message.server)
        if state.voice is None:
            success = await ctx.invoke(self.summon)
            if not success:
                return
        #
        results = self.beets_library.items(args.query)
        if len(results) == 0:
            await self.bot.send_message(ctx.message.channel, "I didn't find any music.")
        else:
            for count in range(args.count):
                if args.random is True:
                    index = random.randint(0, len(results)-1)
                else:
                    index = args.index + count
                try:
                    player = self.get_ffmpeg_player(state, open(results[index].path), results[index].title, results[index].length, pipe=True)
                except IndexError as e:
                    await self.bot.send_message(ctx.message.channel, "You gave me bad index.")
                    return
                except Exception as e:
                    fmt = 'An error occurred while processing this request: ```py\n{}: {}\n```'
                    await self.bot.send_message(ctx.message.channel, fmt.format(type(e).__name__, e))
                else:
                    await self.queue_player(state, ctx.message, player)


    @commands.command(pass_context=True, no_pm=True)
    async def volume(self, ctx, value : int):
        """Sets the volume of the currently playing song."""
        state = self.get_voice_state(ctx.message.server)
        if state.is_playing():
            player = state.player
            player.volume = value / 100
            await self.bot.say("I set the volume to {:.0%}".format(player.volume))

    @commands.command(pass_context=True, no_pm=True)
    async def pause(self, ctx):
        """Pauses the currently played song."""
        state = self.get_voice_state(ctx.message.server)
        if state.is_playing():
            player = state.player
            player.pause()

    @commands.command(pass_context=True, no_pm=True)
    async def resume(self, ctx):
        """Resumes the currently played song."""
        state = self.get_voice_state(ctx.message.server)
        if state.is_playing():
            player = state.player
            player.resume()

    @commands.command(pass_context=True, no_pm=True)
    async def stop(self, ctx):
        """Stops playing audio and leaves the voice channel.
        This also clears the queue.
        """
        server = ctx.message.server
        state = self.get_voice_state(server)

        if state.is_playing():
            player = state.player
            player.stop()

        try:
            state.audio_player.cancel()
            del self.voice_states[server.id]
            await state.voice.disconnect()
        except:
            pass
