
# get images from booru

import random
import math
import shlex
import argparse

import discord
from discord.ext import commands
import aiohttp
import async_timeout
from bs4 import BeautifulSoup

class BooruCog():

    def __init__(self, bot):
        self.bot = bot


    async def fetch(self, session, url):
        with async_timeout.timeout(10):
            async with session.get(url) as response:
                return await response.text()

    @commands.command()
    async def booru(self, *, tags: str):
        """
        Bot give a random image from an imageboard with the given tag.
        --nsfw option may give nsfw image.
        """
        async with aiohttp.ClientSession() as session:
            parser = argparse.ArgumentParser()
            parser.add_argument("--nsfw", action="store_true")
            parser.add_argument("tags", nargs="+")
            try:
                args = parser.parse_args(shlex.split(tags))
            except SystemExit as e:
                await self.bot.say("I don't understand that.")
                return
            if args.nsfw:
                base_url = ""
                url = "http://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=100"
            else:
                base_url = "https:"
                url = "https://safebooru.org/index.php?page=dapi&s=post&q=index&limit=100"
            html = await self.fetch(session, "{}&tags={}".format(url, " ".join(args.tags)))
            # print(html)
            soup = BeautifulSoup(html, "lxml")
            # print(soup.posts["count"], soup.posts["offset"])
            if soup.posts["count"] == "0":
                await self.bot.say("No results for tags {}".format(" ".join(args.tags)))
                return
            index = random.randint(0, int(soup.posts["count"]))
            page = math.floor(index / 100)
            post_index = index % 100 - 1
            # print (index, page, post_index)
            html = await self.fetch(session, "{}&pid={}&tags={}".format(url, page, " ".join(args.tags)))
            soup = BeautifulSoup(html, "lxml")
            print(soup.posts.find_all("post")[post_index]["file_url"])
            await self.bot.say("{}{}".format(base_url, soup.posts.find_all("post")[post_index]["file_url"]))
