
# basic utility commands
import random

import discord
from discord.ext import commands

class UtilCog():

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def id(self, ctx, member : discord.Member = None):
        if member is None:
            await self.bot.say("{} ID: {}".format(ctx.message.author.name, ctx.message.author.id))
        else:
            await self.bot.say("{} ID: {}".format(member.name, member.id))

    @id.error
    async def on_id_error(self, exception, ctx):
        await self.bot.say("I can't find {}.")

    @commands.command()
    async def add(self, left : int, right : int):
        """Adds two numbers together."""
        await self.bot.say(left + right)

    @commands.command(pass_context=True)
    async def roll(self, ctx, limit : str = "100"):
        """Rolls a dice in N format."""
        try:
            limit = int(limit)
        except Exception:
            await self.bot.say('You need to give me a number')
            return
        num_rolls = 1
        if ctx.message.author.id in self.bot.admin_ids:
            print("dice is loaded")
            num_rolls = 5
        result = max([random.randint(0, limit) for _ in range(num_rolls)])
        await self.bot.say(result)

    @commands.command(description='For when you wanna settle the score some other way')
    async def choose(*choices : str):
        """Chooses between multiple choices."""
        await self.bot.say(random.choice(choices))

    @commands.command()
    async def repeat(self, times : int, content='repeating...'):
        """Repeats a message multiple times."""
        for i in range(times):
            await self.bot.say(content)

    @commands.command()
    async def joined(self, member : discord.Member):
        """Says when a member joined."""
        await self.bot.say('{0.name} joined in {0.joined_at}'.format(member))

    @commands.group(pass_context=True)
    async def cool(self, ctx):
        """Says if a user is cool.
        In reality this just checks if a subcommand is being invoked.
        """
        if ctx.invoked_subcommand is None:
            await self.bot.say('No, {0.subcommand_passed} is not cool'.format(ctx))

    @cool.command(name='bot')
    async def _bot(self):
        """Is the bot cool?"""
        await self.bot.say('Yes, the bot is cool.')

    @commands.command()
    async def game(self, game : str):
        await self.bot.change_presence(game=discord.Game(name=game))
