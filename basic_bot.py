import random

import discord
from discord.ext import commands
from discord.ext.commands import Bot

import yaml

from src.booru import BooruCog
from src.utils import UtilCog
from src.sound import SoundCog

class IamARobot(Bot):

    def __init__(self, command_prefix="?", formatter=None, description=None, pm_help=False, admin_ids=[], game=None, **options):
        super().__init__(command_prefix, formatter, description, pm_help, **options)
        self.admin_ids = admin_ids
        self.game = game

    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')
        if self.game:
            self.change_presence(game=discord.Game(name=self.game))


config = yaml.load(open("config.yml", "r"))

description = """
A discord bot.
"""

bot = IamARobot(command_prefix=commands.when_mentioned_or("?"), description=description, admin_ids=config["bot"].get("admin_ids", []), game="Being a robot")

bot.add_cog(UtilCog(bot))
bot.add_cog(BooruCog(bot))
bot.add_cog(SoundCog(bot, config=config["sound"]))
bot.run(config["bot"]["token"])
